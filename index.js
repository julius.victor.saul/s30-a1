const express = require("express");
// require mongoose package
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// Connection to MongoDB Atlas
// Syntax: mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser:true });
mongoose.connect("mongodb+srv://dbjuliussaul:zxc_3000@wdc028-course-booking.6rxug.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

// If a connection error occured, output in the console
// console.error.bind(console) allow us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Schema - determines the structure of the documents to be written in the database
const userSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// Create the User model

const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));

let users =[];

app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);
	}
	else{
		res.send("Please input BOTH username and password");
	}
})

// Create a POST route to create a new user
app.post("/signup", (req, res) => {
	User.findOne({name : req.body.name}, (err, result) => {
		// If a document was found and the document's name matches the information sent via the client/postman
		
		if (result != null && result.name == req.body.name) {
			return res.send("Duplicate user found")
		}
		else{
			let newUser = new User({
				name : req.body.name
			});

			newUser.save((saveErr, savedUser) => {
				// If error occured
				if (saveErr) {
					return console.error(saveErr);
				}
				// If no errors are found
				else{
					return res.status(201).send("New user created")
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`));
